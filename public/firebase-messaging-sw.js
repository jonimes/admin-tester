importScripts("https://www.gstatic.com/firebasejs/8.2.7/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/8.2.7/firebase-messaging.js");

console.log("test service worker inprogress");
// Initialize the Firebase app in the service worker by passing the generated config
try {
  const firebaseConfig = {
    apiKey: "AIzaSyCT4pU-3cp_DMmgWp02Uvf4WpXBsheHJE8",
    authDomain: "switchboard-d42fb.firebaseapp.com",
    projectId: "switchboard-d42fb",
    storageBucket: "switchboard-d42fb.appspot.com",
    messagingSenderId: "1004067988891",
    appId: "1:1004067988891:web:df4feb9fa785b2607fed3e",
    measurementId: "G-LC8KSY2DDK",
  };
  console.log("registration STARTED");

  const app = firebase.initializeApp(firebaseConfig);

  app
    .messaging()
    .getToken({
      vapidkey:
        "BNhgLMvNhbLStLc9rnLHqwSXO-tO1F1EtqyXKWZeG-gsi5qO3H2Gw0bGbEFq17l1Tm-pr07it8O9G1aCPNvbMso",
    })
    .then((currentToken) => {
      if (currentToken) {
        console.log("client token");
      } else {
        console.log(
          "No registration token available. Request permission to generate one."
        );
      }
    })
    .catch((err) => {
      console.log("An error occurred while retrieving token. ", err);
    });

  self.addEventListener("notificationclick", function (event) {
    event.waitUntil(
      clients
        .matchAll({
          type: "window",
        })
        .then((clientList) => {
          for (const client of clientList) {
            if (
              client.url === event.notification.data.FCM_MSG.data.route &&
              "focus" in client
            )
              return client.focus();
          }
          if (clients.openWindow)
            return clients.openWindow("https://www.switchboard.digital/");
          // event.notification.data.FCM_MSG.data.route
        })
    );
  });
} catch (error) {
  console.log("error from -sw.js", error);
}
