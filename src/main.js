import { createApp } from "vue";
import App from "./App.vue";
import router from "./router/index.js";
import { ApolloClients } from "@vue/apollo-composable";
import { createPinia } from "pinia";
import {
  default_apollo_client,
  authorizer_apollo_client,
} from "./plugins/apollo";
import modal from "@/plugins/modal";
import mitt from "@/plugins/mitt";
import piniaPluginPersistedstate from "pinia-plugin-persistedstate";
import VueDOMPurifyHTML from "vue-dompurify-html";
import i18n from "@/plugins/i18n";
import "./index.css";
import "@/helpers/rules";
import VueDragscroll from "vue-dragscroll";
import "leaflet/dist/leaflet.css";
import VueApexCharts from "vue3-apexcharts";
import VueDatePicker from "@vuepic/vue-datepicker";
import "@vuepic/vue-datepicker/dist/main.css";

if ("serviceWorker" in navigator) {
  navigator.serviceWorker
    .register("/firebase-messaging-sw.js")
    .then((reg) => {
      // console.log(`Service Worker Registration (Scope: ${reg.scope})`);
    })
    .catch((error) => {
      const msg = `Service Worker Error (${error})`;
      console.error(msg);
    });
} else {
  // happens when the app isn't served over HTTPS or if the browser doesn't support service workers
  console.warn("Service Worker not available");
}
const pinia = createPinia();
pinia.use(piniaPluginPersistedstate);

const app = createApp(App)
  .use(router)
  .use(pinia)
  .use(mitt)
  .use(VueApexCharts)
  .provide(ApolloClients, {
    default: default_apollo_client,
    authorizer_client: authorizer_apollo_client,
  })
  .use(modal)
  .use(VueDOMPurifyHTML)
  .use(i18n)
  .use(VueDragscroll)
  .component("VueDatePicker", VueDatePicker)
  .mount("#app");
