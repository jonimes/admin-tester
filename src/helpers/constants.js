export default function () {
    return {
        SOCIAL_LINKS: {
            telegram: 'https://t.me/switchboard',
            facebook: 'https://www.facebook.com/switchboard',
            twitter: 'https://twitter.com/switchboard',
            linkedIn: 'https://www.linkedin.com/company/switchboard'
        }
    }
}
