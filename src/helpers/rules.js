import { defineRule } from "vee-validate";
import i18n from "@/plugins/i18n";
import useNotify from "@/use/notify";
import { format, getUnixTime, isWithinInterval } from "date-fns";

const { notify } = useNotify();
defineRule("required", (value, [], context) => {
  if (typeof value === "number") {
    return true;
  }
  // return !!value || i18n.global.t("this_field_is_required");
  return !!value || `Required`;
});

defineRule("email", (value) => {
  return (
    !value ||
    /[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(
      value
    ) ||
    i18n.global.t("Invalid Email")
  );
});

defineRule("password", (value) => {
  return !value || value.length >= 8 || i18n.global.t("password is too short");
});
defineRule("confirmed", (value, [other]) => {
  return !value || value === other || i18n.global.t("Passwords do not match");
});

defineRule("number", (value) => {
  return !value || /^[0-9]+$/.test(value) || i18n.global.t("Numbers Only");
});
defineRule("ethiopian_phone_number", (value) => {
  return !value || /^(7|9)\d{8}$/.test(value) || i18n.global.t("Invalid Phone");
});

defineRule("ageLessThan18", (value) => {
  if (!value) {
    return true;
  }

  if (value < 18) {
    return "Age must be greater than or equal to 18";
  }
  return true;
});

defineRule("ageGreater65", (value) => {
  if (!value) {
    return true;
  }

  if (value > 65) {
    return "Age must be less than or equal to 65";
  }
  return true;
});

defineRule("similar_phone", (value, [other], context) => {
  if (!other) {
    return true;
  } else {
    return `This phone number is already registered`;
  }
});

defineRule("minLength", (value, [], context) => {
  if (value.length >= context.rule.params[0]) {
    return true;
  } else {
    return `${context.field} is must be greater than ${context.rule.params[0]}`;
  }
});

defineRule("minValue", (value, [], context) => {
  if (value >= context.rule.params[0]) {
    return true;
  } else {
    // notify({
    //   title: `Member Required`,
    //   description: `At least ${context.rule.params[0]} ${context.field} is required!`,
    //   cardClass: "bg-red-100",
    // });
    return `At least ${context.rule.params[0]} ${context.field} is required! `;
  }
});

defineRule("password", (value) => {
  return !value || /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$.!%*?&]{6,}$/.test(value) || i18n.global.t("password_not_strong");
});

defineRule("confirm", (value, [other]) => {
  return !value || value === other || i18n.global.t("passwords_do_not_match");
});

defineRule("fileType", (value, [other], context) => {
  let isValid = false;
  for (const i of context.rule.params) {
    if (value[0].replace(" ", "").includes(`${i.replace(" ", "")}`)) {
      isValid = true;
    }
  }
  return isValid || "Not Valid File Format";
});

defineRule("atLeastOne", (value) => {
  if (!value) {
    return true;
  }

  if (value < 1) {
    return "At least select one";
  }
  return true;
});

defineRule("hasOwner", (value) => {
  console.log(
    "this is the length ",
    value.some((e) => e.is_owner == false)
  );
  if (!value) {
    return true;
  }

  if (value.length < 1) {
    return "At least select one";
  } else if (value.some((e) => e.is_owner == false)) {
    return "At least select one Owner";
  }
  return true;
});

defineRule("start_date", (value, [other], context) => {
  let isGreater =
    getUnixTime(new Date(value)) >=
    getUnixTime(new Date(context.rule.params[0]));
  let isInInterval = isWithinInterval(new Date(value), {
    start: new Date(context.rule.params[0]),
    end: new Date(context.rule.params[1]),
  });
  if (value && isGreater && isInInterval) {
    return true;
  }

  return `must be between ${format(
    new Date(context.rule.params[0]),
    "MM/dd/yyyy"
  )} - ${format(new Date(context.rule.params[1]), "MM/dd/yyyy")}`;
});

defineRule("end_date", (value, [other], context) => {
  let isLess =
    getUnixTime(new Date(value)) <=
    getUnixTime(new Date(context.rule.params[1]));
  let isInInterval = isWithinInterval(new Date(value), {
    start: new Date(context.rule.params[0]),
    end: new Date(context.rule.params[1]),
  });
  if (value && isLess && isInInterval) {
    return true;
  }

  return `must be between ${format(
    new Date(context.rule.params[0]),
    "MM/dd/yyyy"
  )} - ${format(new Date(context.rule.params[1]), "MM/dd/yyyy")}`;
});
