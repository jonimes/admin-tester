import { ref, computed } from "vue";
import { defineStore } from "pinia";
import { useStorage } from "@vueuse/core";

export const useUserStore = defineStore(
  "user",
  () => {
    const user = useStorage("session", {}, sessionStorage);
    const authorizer = useStorage("authorizer_session", {}, sessionStorage);
    const isLoggedIn = computed(() => {
      return !!user.value?.custom_token;
    });
    const token = computed(() => {
      return user.value?.custom_token;
    });
    const authorizer_token = computed(() => {
      return authorizer.value?.authorizer_token;
    });
    const email = computed(() => {
      return user.value?.email;
    });
    const name = computed(() => {
      return `${user.value?.given_name}`;
    });
    const surname = computed(() => {
      return `${user.value?.middle_name}`;
    });
    const profile_picture = computed(() => {
      return `${user.value?.picture}`;
    });
    const id = computed(() => {
      return user.value?.sub;
    });

    const updateProfile = (val) => {
      console.log(val.picture);
      user.value.given_name = val.name;
      user.value.middle_name = val.surname;
      if(val.picture){
        user.value.picture = val.picture;
      }
    };

    const set = (res) => {
      let custom_token = res;
      let session = custom_token.split(".")[1];
      session = JSON.parse(window.atob(session));
      session.custom_token = custom_token;
      useStorage("session", {}, sessionStorage);
      user.value = session;
    };
    const setAuthorizer = (res) => {
      let authorizer_token = res;
      let session = authorizer_token.split(".")[1];
      session = JSON.parse(window.atob(session));
      session.authorizer_token = authorizer_token;
      useStorage("authorizer_session", {}, sessionStorage);
      authorizer.value = session;
    };
    const unset = () => {
      authorizer.value = null;
      user.value = null;
    };

    return {
      id,
      name,
      surname,
      profile_picture,
      email,
      user,
      isLoggedIn,
      token,
      authorizer_token,
      authorizer,
      set,
      unset,
      setAuthorizer,
      updateProfile,
    };
  }
  // {
  //   persist: true,
  // }
);
