import { createWebHistory, createRouter } from "vue-router";
import { setupLayouts } from "virtual:generated-layouts";
import { useUserStore } from "@/store/userStore.js";

import generatedRoutes from "virtual:generated-pages";

const routes = setupLayouts(generatedRoutes);

const history = createWebHistory();

const router = createRouter({
  history,
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      return { top: 0 };
    }
  },
});

router.beforeEach((to, from, next) => {
  const userStore = useUserStore();
  if (
    to.fullPath == "/registration-third-party" ||
    to.fullPath == "/get-endorsed"
  ) {
    next("/registration");
  }
  if (!to.matched.length) {
    next("/404");
  }
  if (to.meta.auth && !userStore.isLoggedIn) {
    next("/");
  } else {
    next();
  }
});

router.afterEach(() => {
  // store.loading(false)
});

export default router;
