import {
  ApolloClient,
  createHttpLink,
  split,
  InMemoryCache,
} from "@apollo/client/core";
import { WebSocketLink } from "@apollo/client/link/ws";
import { onError } from "@apollo/client/link/error";
import router from "@/router";
import { getMainDefinition } from "@apollo/client/utilities";
import { ApolloLink, from } from "apollo-link";
import { useUserStore } from "@/store/userStore.js";
import useNotify from "@/use/notify";
import useErrorParser from "@/use/errorParser";

const { notify } = useNotify();
const { parse } = useErrorParser();

const cache = new InMemoryCache({
  addTypename: false,
});

// default client
const default_http_link = createHttpLink({
  uri: import.meta.env.VITE_GRAPH_URL,
});

function getHeaders() {
  const userStore = useUserStore();

  const headers = {};
  // const token = window.localStorage.getItem("session");
  const token = window.sessionStorage.session;
  if (token) {
    headers.authorization = `Bearer ${userStore.token}`;
  }
  return headers;
}

const default_error_link = onError(({ graphQLErrors, networkError }) => {
  const userStore = useUserStore();
  if (
    graphQLErrors &&
    graphQLErrors[0].message === "Could not verify JWT: JWTExpired"
  ) {
    notify({
      title: "Session Expired",
      description: "Session expired please try to login again!",
      cardClass: "bg-primarySb-lite-1",
    });
    userStore.unset();
    router.replace("/");
    return;
  } else if (
    graphQLErrors &&
    graphQLErrors[0].message ==
      "Could not verify JWT: JWSError JWSInvalidSignature"
  ) {
    userStore.unset();
    router.push({ name: "/" });
    return;
  }
});

const default_auth_link = new ApolloLink((operation, forward) => {
  const userStore = useUserStore();

  const { headers } = operation.getContext();

  const h = {
    ...headers,
  };

  if (userStore.isLoggedIn) {
    h.authorization = `Bearer ${userStore.token}`;
  }

  operation.setContext({
    headers: h,
  });

  return forward(operation);
});

const wsLink = new WebSocketLink({
  uri: import.meta.env.VITE_GRAPH_WSS,

  options: {
    reconnect: true,
    lazy: true,
    inactivityTimeout: 30000,
    // connectionParams: {
    //   headers: {
    //     authorization: `Bearer ${
    //       JSON.parse(sessionStorage.session || null)?.custom_token || null
    //     }`,
    //   },
    // },
    connectionParams: () => {
      return { headers: getHeaders() };
    },
  },
});

const link = split(
  ({ query }) => {
    const definition = getMainDefinition(query);
    return (
      definition.kind === "OperationDefinition" &&
      definition.operation === "subscription"
    );
  },

  wsLink,
  default_auth_link.concat(default_error_link.concat(default_http_link))
);

const default_apollo_client = new ApolloClient({
  cache,
  link,
});

// const authorizer_auth_link = new ApolloLink((operation, forward) => {
//   const userStore = useUserStore();
//   const { headers } = operation.getContext();

//   const h = {
//     ...headers,
//   };

//   if (userStore.isLoggedIn) {
//     h.authorization = `Bearer ${userStore.authorizer_token}`;
//   }

//   operation.setContext({
//     headers: h,
//   });

//   return forward(operation);
// });

// const authorizer_apollo_client = new ApolloClient({
//   link: from([authorizer_auth_link, default_error_link, default_http_link]),
//   cache,
// });

const authorizer_auth_link = new ApolloLink((operation, forward) => {
  const userStore = useUserStore();
  const { headers } = operation.getContext();

  const h = {
    ...headers,
  };

  if (userStore.isLoggedIn) {
    h.authorization = `Bearer ${userStore.authorizer_token}`;
  }

  operation.setContext({
    headers: h,
  });

  return forward(operation);
});

const authorizer_apollo_client = new ApolloClient({
  link: from([authorizer_auth_link, default_error_link, default_http_link]),
  cache,
});

export { default_apollo_client, authorizer_apollo_client };
