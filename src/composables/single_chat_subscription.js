import { useQuery } from "@vue/apollo-composable";

export default function (query, filter, limit, offset, enabled) {
  const {
    result,
    onResult,
    onError,
    loading,
    refetch,
    fetchMore,
    subscribeToMore,
  } = useQuery(
    query,
    () => ({
      filter: filter.value,
      limit: limit.value,
      offset: offset.value,
    }),
    () => ({
      fetchPolicy: "network-only",
      enabled: enabled.value,
    })
  );

  return {
    onResult,
    loading,
    onError,
    refetch,
    fetchMore,
    subscribeToMore,
  };
}
