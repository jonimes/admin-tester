import { useQuery } from "@vue/apollo-composable";
import query from "@/queries/individual/community_leader_aggregate.gql";
import {
  pending,
  accepted,
  rejected,
} from "@/composables/community_aggregate_filter.js";

const role = JSON.parse(sessionStorage.session).metadata.roles.find(
  (e) => e == "role:system:admin"
);

export default function () {
  const { onResult, onError, loading, refetch } = useQuery(
    query,
    () => ({
      pending: pending,
      accepted: accepted,
      rejected: rejected,
    }),
    () => ({
      fetchPolicy: "network-only",
      context: {
        headers: {
          "x-hasura-role": role,
        },
      },
    })
  );

  return {
    onResult,
    loading,
    refetch,
    onError,
  };
}
