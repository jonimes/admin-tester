export const underReview = {
  is_published: { _eq: false },
  is_closed: { _eq: false },
  is_rejected: { _eq: false },
  is_deleted: { _eq: false },
};

export const active = {
  is_published: { _eq: true },
  is_rejected: { _eq: false },
  is_suspended: { _eq: false },
  is_completed: { _eq: false },
  is_deleted: { _eq: false },
};

export const completed = {
  is_published: { _eq: true },
  is_completed: { _eq: true },
  is_rejected: { _eq: false },
  is_deleted: { _eq: false },
};

export const closed = {
  is_closed: { _eq: true },
  is_deleted: { _eq: false },
};

export const suspended = {
  is_suspended: { _eq: true },
  is_deleted: { _eq: false },
};

export const declined = {
  is_published: { _eq: false },
  is_rejected: { _eq: true },
  is_deleted: { _eq: false },
};

export const deleted = {
  is_deleted: { _eq: true },
};
