import { useMutation } from "@vue/apollo-composable";

export default function (query) {
  const { mutate, onDone, loading, onError } = useMutation(query, () => ({
    fetchPolicy: "network-only",
  }));

  return {
    mutate,
    loading,
    onDone,
    onError,
  };
}
