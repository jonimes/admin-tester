import { ref } from "vue";
import { useSubscription } from "@vue/apollo-composable";

const enable = ref(true);
const limit_ = ref(100);

const role = JSON.parse(sessionStorage.session).metadata.roles.find(
  (e) => e == "role:system:admin"
);

export default function (query, filter, limit = limit_, enabled = enable) {
  const { onResult, onError, loading, refetch } = useSubscription(
    query,
    () => ({
      limit: limit && limit.value ? limit.value : undefined,
      filter: filter.value,
    }),
    () => ({
      fetchPolicy: "network-only",
      context: {
        headers: {
          "x-hasura-role": role,
        },
      },
      enabled: enabled.value,
    })
  );

  return {
    onResult,
    loading,
    refetch,
    onError,
  };
}
