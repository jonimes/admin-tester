export const pending = {
  user: {
    is_registration_completed: {
      _eq: true,
    },
  },
  _and: [
    {
      _or: [
        {
          is_accepted: {
            _eq: false,
          },
        },
        {
          is_accepted: {
            _is_null: true,
          },
        },
      ],
    },
    {
      _or: [
        {
          is_rejected: {
            _eq: false,
          },
        },
        {
          is_rejected: {
            _is_null: true,
          },
        },
      ],
    },
  ],
};

export const accepted = {
  user: {
    is_registration_completed: {
      _eq: true,
    },
  },
  is_accepted: {
    _eq: true,
  },
  _or: [
    {
      is_rejected: {
        _eq: false,
      },
    },
    {
      is_rejected: {
        _is_null: true,
      },
    },
  ],
};

export const rejected = {
  user: {
    is_registration_completed: {
      _eq: true,
    },
  },
  is_rejected: {
    _eq: true,
  },
  _or: [
    {
      is_accepted: {
        _eq: false,
      },
    },
    {
      is_accepted: {
        _is_null: true,
      },
    },
  ],
};
