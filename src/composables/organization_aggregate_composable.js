import { useQuery } from "@vue/apollo-composable";
import organization_aggregate from "@/queries/organization/organization_aggregate.gql";
import {
  underReview,
  active,
  suspended,
  rejected,
  closed,
  deleted,
} from "@/composables/organization_aggregate_filter.js";

const role = JSON.parse(sessionStorage.session).metadata.roles.find(
  (e) => e == "role:system:admin"
);

export default function () {
  const { onResult, onError, loading, refetch } = useQuery(
    organization_aggregate,
    () => ({
      underReview: underReview,
      active: active,
      suspended: suspended,
      rejected: rejected,
      closed: closed,
      deleted: deleted,
    }),
    () => ({
      fetchPolicy: "network-only",
      context: {
        headers: {
          "x-hasura-role": role,
        },
      },
    })
  );

  return {
    onResult,
    loading,
    refetch,
    onError,
  };
}
