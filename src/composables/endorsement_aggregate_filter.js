export const pending = {
  endorsee: {
    is_registration_completed: {
      _eq: true,
    },
    // is_endoresed: {
    //   _eq: false,
    // },
  },
  is_pending: {
    _eq: true,
  },
  is_rejected: {
    _eq: false,
  },
  is_accepted: {
    _eq: false,
  },
};

export const accepted = {
  endorsee: {
    is_registration_completed: {
      _eq: true,
    },
    // is_endoresed: {
    //   _eq: true,
    // },
  },
  is_accepted: {
    _eq: true,
  },
  is_rejected: {
    _eq: false,
  },
  is_pending: {
    _eq: false,
  },
};

export const rejected = {
  endorsee: {
    is_registration_completed: {
      _eq: true,
    },
    // is_endoresed: {
    //   _eq: false,
    // },
  },
  is_accepted: {
    _eq: false,
  },
  is_rejected: {
    _eq: true,
  },
};

export const draft = {
  is_registration_completed: {
    _eq: true,
  },
  is_active: {
    _eq: false,
  },
  is_endoresed: {
    _eq: false,
  },
  is_blocked: {
    _eq: false,
  },
  is_deleted: {
    _eq: false,
  },
};
