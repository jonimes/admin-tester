import { ref } from "vue";
import { useQuery } from "@vue/apollo-composable";

const enable = ref(true);

const role = JSON.parse(sessionStorage.session).metadata.roles.find(
  (e) => e == "role:system:admin"
);

export default function (query, filter, enabled = enable) {
  const { onResult, onError, loading, refetch } = useQuery(
    query,
    () => ({
      id: filter.value,
    }),
    () => ({
      fetchPolicy: "network-only",
      context: {
        headers: {
          "x-hasura-role": role,
        },
      },
      enabled: enabled.value,
    })
  );

  return {
    onResult,
    loading,
    refetch,
    onError,
  };
}
