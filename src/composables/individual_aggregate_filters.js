export const active_individual = {
  is_registration_completed: {
    _eq: true,
  },
  // _or: [
  //   {
  //     is_paused: {
  //       _eq: false,
  //     },
  //   },
  //   {
  //     is_paused: {
  //       _is_null: true,
  //     },
  //   },
  // ],
  // is_deleted: {
  //   _eq: false,
  // },
  // is_blocked: {
  //   _eq: false,
  // },
  is_endoresed: {
    _eq: true,
  },
  is_active: {
    _eq: true,
  },
};

export const paused_individuals = {
  is_registration_completed: {
    _eq: true,
  },
  is_paused: {
    _eq: true,
  },
  is_active: {
    _eq: false,
  },
  is_endoresed: {
    _eq: true,
  },
};

export const blocked_individuals = {
  is_registration_completed: {
    _eq: true,
  },
  is_deleted: {
    _eq: false,
  },
  is_active: {
    _eq: false,
  },
  is_blocked: {
    _eq: true,
  },
};

export const deleted_individuals = {
  is_registration_completed: {
    _eq: true,
  },
  is_deleted: {
    _eq: true,
  },
};
