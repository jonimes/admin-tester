import { useSubscription } from "@vue/apollo-composable";

export default function (query, enabled) {
  const { onResult, onError, loading } = useSubscription(
    query,
    null,
    () => ({
      fetchPolicy: "network-only",
      enabled: enabled.value,
    })
  );

  return {
    onResult,
    loading,
    onError,
  };
}
