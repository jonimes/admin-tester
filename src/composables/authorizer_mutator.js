import { useMutation } from "@vue/apollo-composable";

export default function (query) {
  const { mutate, onDone, loading, onError } = useMutation(query, () => ({
    fetchPolicy: "network-only",
    clientId: "authorizer_client",
    context: {
      headers: {
        "x-hasura-role": "",
      },
    },
  }));
  return {
    mutate,
    loading,
    onDone,
    onError,
  };
}
