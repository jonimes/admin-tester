import { useQuery } from "@vue/apollo-composable";
import organization_aggregate from "@/queries/organization/request_aggregate.gql";
import {
  underReview,
  active,
  completed,
  closed,
  suspended,
  declined,
  deleted,
} from "@/composables/request_aggregate_filter.js";

const role = JSON.parse(sessionStorage.session).metadata.roles.find(
  (e) => e == "role:system:admin"
);

export default function () {
  const { onResult, onError, loading, refetch } = useQuery(
    organization_aggregate,
    () => ({
      underReview: underReview,
      active: active,
      completed: completed,
      closed: closed,
      declined: declined,
      suspended: suspended,
      deleted: deleted,
    }),
    () => ({
      fetchPolicy: "network-only",
      context: {
        headers: {
          "x-hasura-role": role,
        },
      },
    })
  );

  return {
    onResult,
    loading,
    refetch,
    onError,
  };
}
