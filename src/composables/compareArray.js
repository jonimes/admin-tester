import { ref, onMounted, onUnmounted } from "vue";

export function compareArray(incoming, tobeSent, compareBy) {
  let oldData = new Map(incoming.map((item) => [item[compareBy], item]));
  let newData = new Map(tobeSent.map((item) => [item[compareBy], item]));
  let final = [];
  function test(value, key, map) {
    if (!newData.has(key)) {
      final.push(value);
    }
  }
  oldData.forEach(test);
  return final;
}