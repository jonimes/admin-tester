import { useMutation } from "@vue/apollo-composable";

const role = "role:user";

export default function (query) {
  const { mutate, onDone, loading, onError } = useMutation(query, () => ({
    fetchPolicy: "network-only",
    // clientId: "entity",
    context: {
      headers: {
        "x-hasura-role": role,
      },
    },
  }));
  return {
    mutate,
    loading,
    onDone,
    onError,
  };
}
