import { useQuery } from "@vue/apollo-composable";

export default function (query, filter, order, limit, offset, enabled) {
  const { onResult, onError, loading, refetch } = useQuery(
    query,
    () => ({
      limit: limit && limit.value ? limit.value : undefined,
      filter: filter.value,
      order: order && order.value.length ? order.value : undefined,
      offset: offset && offset.value ? offset.value : undefined,
    }),
    () => ({
      fetchPolicy: "network-only",
      enabled: enabled.value,
    })
  );

  return {
    onResult,
    loading,
    refetch,
    onError,
  };
}
