export const underReview = {
  is_approved: {
    _eq: false,
  },
  is_deleted: {
    _eq: false,
  },
  _or: [
    {
      is_rejected: {
        _eq: false,
      },
    },
    {
      is_rejected: {
        _is_null: true,
      },
    },
  ],
};

export const active = {
  is_approved: {
    _eq: true,
  },
  is_active: {
    _eq: true,
  },
  is_closed: {
    _eq: false,
  },
  _or: [
    {
      is_rejected: {
        _eq: false,
      },
    },
    {
      is_rejected: {
        _is_null: true,
      },
    },
  ],
};

export const suspended = {
  is_approved: {
    _eq: true,
  },
  is_active: {
    _eq: false,
  },
  is_suspended: {
    _eq: true,
  },
};

export const rejected = {
  is_approved: {
    _eq: false,
  },
  is_rejected: {
    _eq: true,
  },
};

export const closed = {
  is_closed: {
    _eq: true,
  },
};

export const deleted = {
  is_deleted: {
    _eq: true,
  },
};
