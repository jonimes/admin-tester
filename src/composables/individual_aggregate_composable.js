import { useQuery } from "@vue/apollo-composable";
import individual_aggregate from "@/queries/individual/individual_aggregates.gql";
import {
  active_individual,
  paused_individuals,
  blocked_individuals,
  deleted_individuals,
} from "@/composables/individual_aggregate_filters.js";

const role = JSON.parse(sessionStorage.session).metadata.roles.find(
  (e) => e == "role:system:admin"
);

export default function () {
  const { onResult, onError, loading, refetch } = useQuery(
    individual_aggregate,
    () => ({
      activeFilter: active_individual,
      pausedFilter: paused_individuals,
      blockedFilter: blocked_individuals,
      deletedFilter: deleted_individuals,
    }),
    () => ({
      fetchPolicy: "network-only",
      context: {
        headers: {
          "x-hasura-role": role,
        },
      },
    })
  );

  return {
    onResult,
    loading,
    refetch,
    onError,
  };
}
