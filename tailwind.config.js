const path = require("path");
module.exports = {
  darkMode: "class",
  mode: "jit",
  content: [
    "./index.html",
    "./public/**/*.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    screens: {
      xxs: "380px",
      // => @media (min-width: 380px) { ... }

      xs: "512px",
      // => @media (min-width: 512px) { ... }

      sm: "640px",
      // => @media (min-width: 640px) { ... }

      md: "768px",
      // => @media (min-width: 768px) { ... }

      lg: "1024px",
      // => @media (min-width: 1024px) { ... }

      xl: "1280px",
      // => @media (min-width: 1280px) { ... }

      "2xl": "1536px",
      // => @media (min-width: 1536px) { ... }

      "3xl": "1600px",
      // => @media (min-width: 1600px) { ... }

      "4xl": "1920px",
      // => @media (min-width: 1920px) { ... }
    },
    fontFamily: {
      body: ["Roboto"],
      diplay: ["Roboto"],
      sans: ["Roboto"],
      serif: ["Roboto"],
    },
    aspectRatio: {
      auto: "auto",
      square: "1 / 1",
      video: "16 / 9",
      1: "1",
      2: "2",
      3: "3",
      4: "4",
      5: "5",
      6: "6",
      7: "7",
      8: "8",
      9: "9",
      10: "10",
      11: "11",
      12: "12",
      13: "13",
      14: "14",
      15: "15",
      16: "16",
    },

    extend: {
      backgroundImage: {
        loginSideTexture: "url('/images/loginSideImg.png')",
      },
      lineClamp: {
        7: "7",
        8: "8",
        9: "9",
        10: "10",
      },
      fontSize: {
        md: ["1.125rem", "1.5rem"],
      },
      maxWidth: {
        six: "102rem",
      },
      height: {
        nav: "5.25rem",
        sideHeight: "calc(100vh - 70px)",
        configHeight: "calc(100vh - 120px)",
      },
      width: {
        sideWidth: "calc(100vw - 85px)",
      },
      flex: {
        six: "1 0 16.6%",
      },
      dropShadow: {
        full: "0 3px 1px -2px rgba(0,0,0, 0.2), 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12) ",
      },
      colors: {
        primary: "#203468",
        "primary-2": "#80CBC4",
        "primary-3": "#009688",
        "primary-4": "#B3E0DB",
        "primary-5": "#E6F5F3",
        "primary-6": "#045750",
        "primary-lite": "#0D3972",
        "primary-lite-1": "#A7C6ED",
        "primary-lite-2": "#F6F9FD",
        secondary: "#444f60",
        "secondary-2": "#556987",
        "secondary-3": "#C1C1C1",
        "secondary-4": "#C7CACF",
        "secondary-5": "#444444",
        "secondary-6": "#697280",
        "secondary-7": "#ECECEC",
        "secondary-8": "#ffffff",
        "secondary-9": "#596474",
        "secondary-10": "#E5E5E5",
        "secondary-11": "#E1E1E1",
        "secondary-lite": "#ffffff",
        "secondary-lite-2": "#f5f5f5",
        "secondary-lite-3": "#F9FAFB",
        "secondary-dark": "#313131",
        "hahu-gray": "#444F60",
        "hahu-border": "#D1D5DB",
        "hahu-red": "#991B1B",
        "hahu-red-2": "#DC2626",
        "hahu-red-3": "#FEE2E2",
        "hahu-blue": "#2563EB",

        // dark theme colors

        "primary-dark": "#002F6C",
        "primary-dark-2": "#ECEDEF",
        "secondary-dark-2": "#1B2637",

        // SwithBoard
        primarySb: "#002F6C",
        "primarySb-1": "#1A447B",
        "primarySb-2": "#335989",
        "primarySb-3": "#4D6D98",
        "primarySb-4": "#E6EAF0",

        "primarySb-lite": "#A7C6ED",
        "primarySb-lite-1": "#B0CCEF",
        "primarySb-lite-2": "#B9D1F1",
        "primarySb-lite-3": "#E5EEFA",
        "primarySb-lite-4": "#F6F9FD",
      },
    },
  },
  variants: {
    extend: {
      backgroundOpacity: ["disabled"],
      cursor: ["disabled"],
      borderWidth: ["last", "focus-within", "first"],
      borderRadius: ["hover"],
    },
  },
  plugins: [
    require("@tailwindcss/forms"),
    // require('@tailwindcss/typography'),
    require("@tailwindcss/line-clamp"),
    require("@tailwindcss/aspect-ratio"),
    require("@tailwindcss/container-queries"),
  ],
};
